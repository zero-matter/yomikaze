#!/bin/sh

npx tailwindcss build \
    --config ./tailwind/tailwind.config.js \
    --content "./**/*.{html,js}" \
    --input ./tailwind/tailwind.input.css \
    --output ./assets/css/tailwind.css \
    --watch \
    --minify