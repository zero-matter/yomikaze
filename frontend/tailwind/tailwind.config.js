/** @type {import('tailwindcss').Config} */
module.exports = {
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        "ayu-dark": {
          "primary": "#39BAE6",
          "secondary": "#7FD962",
          "accent": "#E6B450",
          "neutral": "#ACB6BF",
          "base-100": "#0F131A",
          "base-200": "#0D1017",
          "base-300": "#0B0E14",
          "info": "#59C2FF",
          "success": "#7FD962",
          "warning": "#FFB454",
          "error": "#D95757",
        },
      },
    ],
  },
}
