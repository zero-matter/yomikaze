package io.gitlab.zeromatter.yomikaze;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YomikazeApplication {

    public static void main(String[] args) {
        SpringApplication.run(YomikazeApplication.class, args);
    }

}
