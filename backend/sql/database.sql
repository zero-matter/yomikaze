 
-------------
-- CLEANUP --
-------------

drop table if exists account            cascade;
drop table if exists gender             cascade;
drop table if exists profile            cascade;
drop table if exists permission         cascade;
drop table if exists role               cascade;
drop table if exists role_permission    cascade;
drop table if exists account_role       cascade;
drop table if exists account_permission cascade;
drop table if exists comic              cascade;
drop table if exists comic_chapter      cascade;
drop table if exists comic_chapter_page cascade;
drop table if exists genre              cascade;
drop table if exists comic_genre        cascade;
drop table if exists list               cascade;
drop table if exists list_item          cascade;
drop table if exists comment            cascade;
drop table if exists comment_vote       cascade;
drop table if exists list_vote          cascade;
drop table if exists comic_rating       cascade;
drop table if exists history            cascade;

-------------------
-- CREATE TABLES --
-------------------


-->> account & profile <<--

create table account
(
    id         serial                 not null primary key,
    username   character varying(256) not null unique,
    email      character varying(256) not null unique,
    password   character varying(256) not null,
    two_factor character varying(128)          default null,
    jwt_secret character varying(128) not null default gen_random_uuid()
);

create table gender
(
    id   serial                not null primary key,
    name character varying(32) not null unique
);
insert into gender(name)
values ('Male'),
       ('Female'),
       ('Other');

create table profile
(
    account_id      int                    not null primary key references account(id) on delete cascade,
    display_name    character varying(256) default null,
    profile_picture character varying(256) default null,
    bio             character varying(256) default null,
    birthday        timestamp              default null,
    gender          int                    default null
        references gender(id)
            on delete set default
);


-->> roles & permissions <<--

create table permission
(
    id          serial                 not null primary key,
    name        character varying(256) not null unique,
    description character varying(256) default null
);

create table role
(
    id          serial                 not null primary key,
    name        character varying(256) not null unique,
    description character varying(256) default null
);

create table role_permission
(
    role_id       int not null references role(id)       on delete cascade,
    permission_id int not null references permission(id) on delete cascade
);

create table account_role
(
    account_id int not null references account(id) on delete cascade,
    role_id    int not null references role(id)    on delete cascade
);

create table account_permission
(
    account_id    int not null references account(id)    on delete cascade,
    permission_id int not null references permission(id) on delete cascade
);

create or replace function get_role_permissions(role_name character varying)
    returns setof permission
    as $$
    select distinct p.* from role r
        join role_permission rp on rp.role_id = r.id
        join permission p on p.id = rp.permission_id
    where r.name = $1
    $$ language sql stable;

create or replace function get_account_role(account_id int)
    returns setof role
    as $$
    select r.* from account a
        join account_role ar on ar.account_id = a.id
        join role r on r.id = ar.role_id
    where a.id = $1
    $$ language sql stable;

create or replace function get_account_permissions(account_id int)
    returns setof permission
    as $$
    select distinct p.* from account a
        join account_permission ap on ap.account_id = a.id
        join account_role ar on ar.account_id = a.id
        join role r on r.id = ar.role_id
        join role_permission rp on rp.role_id = r.id
        join permission p on p.id in (ap.permission_id, rp.permission_id)
    where a.id = $1
    $$ language sql stable;


-->> comic <<--

create table comic
(
    id            serial                  not null primary key,
    title         character varying(256)  not null,
    description   character varying(1024) default null,
    cover_picture character varying(256)  default null,
    published     timestamp               default null,
    finished      timestamp               default null,
    created_at    timestamp               not null default now(),
    updated_at    timestamp               not null default now()
);

create table comic_chapter
(
    id         serial                 not null primary key,
    comic_id   int                    not null references comic(id) on delete cascade,
    title      character varying(256) not null,
    ordinal    int                    not null default -1,
    created_at timestamp              not null default now(),
    updated_at timestamp              default null
);

create table comic_chapter_page
(
    id         serial                 not null primary key,
    chapter_id int                    not null references comic_chapter(id) on delete cascade,
    picture    character varying(256) not null,
    ordinal    int                    not null default -1,
    created_at timestamp              not null default now()
);

create table genre
(
    id          serial                  not null primary key,
    name        character varying(256)  not null unique,
    description character varying(1024) default null
);

create table comic_genre
(
    comic_id int not null references comic(id) on delete cascade,
    genre_id int not null references genre(id) on delete cascade
);


-->> account x comic <<--

create table comic_rating
(
    id         serial                 not null primary key,
    comic_id   int                    not null references comic(id)   on delete cascade,
    account_id int                    not null references account(id) on delete cascade,
    rating     int                    not null check ( rating in (1, 2, 3, 4, 5) ),
    review     character varying(256) default null,
    created_at timestamp              not null default now()
);

create table history
(
    id         serial    not null primary key,
    account_id int       not null references account(id)       on delete cascade,
    chapter_id int       not null references comic_chapter(id) on delete set null,
    created_at timestamp not null default now()
);

create table list
(
    id         serial                 not null primary key,
    account_id int                    not null references account(id) on delete cascade,
    name       character varying(256) not null,
    created_at timestamp              not null default now(),
    updated_at timestamp              not null default now(),
    public     boolean                not null default false
);

create table list_item
(
    id         serial    not null primary key,
    list_id    int       not null references list(id)  on delete cascade,
    comic_id   int       not null references comic(id) on delete cascade,
    ordinal    int       not null default -1,
    created_at timestamp not null default now()
);

create table list_vote
(
    id         serial    not null primary key,
    list_id    int       not null references list(id)    on delete cascade,
    account_id int       not null references account(id) on delete cascade,
    vote       int       not null check ( vote in (-1, 1) ),
    created_at timestamp not null default now()
);

create table comment
(
    id         serial                  not null primary key,
    reply_to   int                     default null references comment(id)            on delete cascade,
    chapter_id int                     default null references comic_chapter_page(id) on delete set null,
    comic_id   int                     not null     references comic(id)              on delete cascade,
    account_id int                     not null     references account(id)            on delete cascade,
    content    character varying(1024) not null,
    created_at timestamp               not null default now(),
    updated_at timestamp               default null
);

create table comment_vote
(
    id         serial    not null primary key,
    comment_id int       not null references comment(id) on delete cascade,
    account_id int       not null references account(id) on delete cascade,
    vote       int       not null check ( vote in (-1, 1) ),
    created_at timestamp not null default now()
);

-------------------------------------
-- FUNCTIONS, PROCEDURES, TRIGGERS --
-------------------------------------

create or replace function get_latest_comic_chapter(comic_id int)
    returns int
    as $$;
        select ordinal from comic_chapter where comic_chapter.comic_id = $1 order by ordinal desc limit 1
    $$ language sql stable;

create or replace function get_latest_comic_chapter_pages(chapter_id int)
    returns int
    as $$
        select ordinal from comic_chapter_page where comic_chapter_page.chapter_id = $1 order by ordinal desc limit 1
    $$ language sql stable;

create or replace function on_add_comic_chapter()
    returns trigger
    as $$
    begin
        update comic set updated_at = now() where id = new.comic_id;
        if new.ordinal = -1 then
            new.ordinal := get_latest_comic_chapter(new.comic_id) + 1;
        end if;
        return new;
    end;
    $$ language plpgsql;

create or replace trigger before_add_comic_chapter
    before insert on comic_chapter
    for each row execute procedure on_add_comic_chapter();

create or replace function on_add_new_comic_chapter_page()
    returns trigger
    as $$
    begin
        update comic_chapter set updated_at = now() where id = new.chapter_id;
        if new.ordinal = -1 then
            new.ordinal := get_latest_comic_chapter_pages(new.chapter_id) + 1;
        end if;
        return new;
    end;
    $$ language plpgsql;

create or replace trigger before_add_new_comic_chapter_page
    before insert on comic_chapter_page
    for each row execute procedure on_add_new_comic_chapter_page();

create or replace function get_comic_chapters(comic_id int)
    returns setof comic_chapter
    as $$
        select *
        from comic_chapter
        where comic_chapter.comic_id = get_comic_chapters.comic_id
        order by ordinal, created_at, updated_at
    $$ language sql stable;

create or replace function get_comic_chapter_pages(chapter_id int)
    returns setof comic_chapter_page
    as $$
        select *
        from comic_chapter_page
        where comic_chapter_page.chapter_id = get_comic_chapter_pages.chapter_id
        order by ordinal, created_at
    $$ language sql stable;

create or replace function on_delete_chapter()
    returns trigger
    as $$
    begin
        update comic set updated_at = now() where id = old.comic_id;
        update comic_chapter set ordinal = ordinal - 1 where comic_id = old.comic_id and ordinal > old.ordinal;
    end;
    $$ language plpgsql;

create or replace trigger before_delete_chapter
    before delete on comic_chapter
    for each row execute procedure on_delete_chapter();

create or replace function on_delete_chapter_page()
    returns trigger
    as $$
    begin
        update comic_chapter set updated_at = now() where id = old.chapter_id;
        update comic_chapter_page set ordinal = ordinal - 1 where chapter_id = old.chapter_id and ordinal > old.ordinal;
    end;
    $$ language plpgsql;

create or replace trigger before_delete_chapter_page
    before delete on comic_chapter_page
    for each row execute procedure on_delete_chapter_page();

create or replace function on_account_created()
    returns trigger
    as $$
    begin
        insert into profile (account_id) values (new.id);
        insert into list (account_id, name, public) values (new.id, 'Bookmark', false);
    end;
    $$ language plpgsql;

create or replace trigger after_account_created
    after insert on account
    for each row execute procedure on_account_created();

create or replace function get_latest_list_item(list_id int)
    returns int
    as $$
        select ordinal from list_item where list_item.list_id = $1 order by ordinal desc limit 1
    $$ language sql stable;

create or replace function on_add_list_item()
    returns trigger
    as $$
    begin
        update list set updated_at = now() where id = new.list_id;
        if new.ordinal = -1 then
            new.ordinal := get_latest_list_item(new.list_id) + 1;
        end if;
        return new;
    end;
    $$ language plpgsql;

create or replace trigger before_add_list_item
    before insert on list_item
    for each row execute procedure on_add_list_item();