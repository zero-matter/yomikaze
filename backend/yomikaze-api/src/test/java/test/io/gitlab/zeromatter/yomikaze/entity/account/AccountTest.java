package test.io.gitlab.zeromatter.yomikaze.entity.account;

import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.gitlab.k4zoku.core.database.sql.base.SQLClient;
import io.gitlab.zeromatter.yomikaze.database.DatabaseContext;
import io.gitlab.zeromatter.yomikaze.entity.account.Account;
import io.gitlab.zeromatter.yomikaze.entity.account.AccountDataAccess;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AccountTest {

    private static final Random RANDOM = new Random();
    private static final int maxAccounts = 10;
    private static SQLClient client;
    private static AccountDataAccess dataAccess;

    private static final String TABLE = "account_test";

    @BeforeAll
    static void init() throws SQLException {
        client = DatabaseContext.getSQLClient();
        try (Connection connection = client.getConnection()) {
            String sql = "create table if not exists " + TABLE + " (" +
                    "id serial not null primary key, " +
                    "username varchar(64) not null unique, " +
                    "email varchar(256) not null unique, " +
                    "password varchar(128) not null, " +
                    "two_factor boolean not null default false, " +
                    "created_at timestamp not null default now(), " +
                    "secret varchar(128) not null default gen_random_uuid()" +
                    ");";
            Statement statement = connection.createStatement();
            statement.execute(sql);
            statement.close();
        }
        dataAccess = new AccountDataAccess("account_test");
    }

    @AfterAll
    static void cleanup() throws SQLException {
        try (Connection connection = client.getConnection()) {
            String sql = "drop table if exists " + TABLE + " cascade";
            Statement statement = connection.createStatement();
            statement.execute(sql);
            statement.close();
        }
        client.close();
    }

    @Test
    @Order(1)
    void registerTest() {
        for (int i = 0; i < maxAccounts; i++) {
            Account account = dataAccess.signUp("username" + i, "email" + i + "@email.com", "password" + i);
            assertNotEquals(-1, account.getId());
            assertNotNull(account.getCreatedAt());
            assertEquals(account, dataAccess.find(account.getId()));
        }
    }

    @Test
    @Order(2)
    void loginTest() {
        for (int i = 0; i < maxAccounts; i++) {
            Account account = dataAccess.signIn("username" + i, "password" + i);
            assertNotNull(account);
            assertEquals(i + 1, account.getId());
            assertEquals("username" + i, account.getUsername());
            assertEquals("email" + i + "@email.com", account.getEmail());
            assertTrue(account.verifyPassword("password" + i));

            String token = account.generateToken();
            DecodedJWT decoded = account.verifyToken(token);
            assertEquals(account.getId(), decoded.getClaim("id").asInt());
            assertEquals(account.getUsername(), decoded.getClaim("username").asString());
            assertEquals(account.getEmail(), decoded.getClaim("email").asString());
            dataAccess.signOut(account);
            assertThrows(SignatureVerificationException.class, () -> account.verifyToken(token));
        }
    }

    @Test
    @Order(3)
    void listingTest() {
        int limit = RANDOM.nextInt(maxAccounts) + 1;
        int offset = RANDOM.nextInt(maxAccounts - 3) + 1;
        List<Account> accounts = dataAccess.list(limit, offset);
        assertEquals(Math.min(maxAccounts - offset, limit), accounts.size());
        assertEquals(offset + 1, accounts.get(0).getId());
    }

    @Test
    @Order(4)
    void editTest() {
        int id = RANDOM.nextInt(maxAccounts - 1) + 1;
        Account account = dataAccess.find(id);
        assertNotNull(account);
        account.setUsername("new_username");
        account.setEmail("new_email");
        account.setRawPassword("new_password");
        account.setTwoFactorEnabled(true);
        dataAccess.update(account);
        Account newAccount = dataAccess.find(account.getId());
        assertNotNull(newAccount);
        assertEquals("new_username", newAccount.getUsername());
        assertEquals("new_email", newAccount.getEmail());
        assertTrue(newAccount.verifyPassword("new_password"));
        assertTrue(newAccount.isTwoFactorEnabled());
    }

    @Test
    @Order(5)
    void deleteTest() {
        // pick a random account to delete
        int id = RANDOM.nextInt(maxAccounts - 1) + 1;
        // verify that the account exists
        assertNotNull(dataAccess.find(id));
        // delete the account
        dataAccess.remove(id);
        // verify that the account is deleted
        assertNull(dataAccess.find(id));
    }
}
