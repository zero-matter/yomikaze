package io.gitlab.zeromatter.yomikaze.database;

import io.gitlab.k4zoku.core.database.sql.base.SQLClient;
import io.gitlab.k4zoku.core.database.sql.base.SQLDriver;
import io.gitlab.k4zoku.core.database.sql.base.SQLSettings;
import io.gitlab.k4zoku.core.database.sql.client.JavaSQLClient;
import io.gitlab.k4zoku.core.database.sql.driver.PostgreSQLDriver;

import java.util.Optional;

public class DatabaseContext {

    public static final String DEFAULT_DATABASE_HOST = "127.0.0.1";
    public static final String DEFAULT_DATABASE_PORT = "5432";
    public static final String DEFAULT_DATABASE_NAME = "yomikaze";
    public static final String DEFAULT_DATABASE_USER = "yomikaze";
    public static final String DEFAULT_DATABASE_PASS = "";
    private static DatabaseContext instance;
    private final SQLClient client;

    private DatabaseContext() {
        String host = Optional.ofNullable(System.getenv("DB_HOST")).filter(h -> !h.isEmpty()).orElse(DEFAULT_DATABASE_HOST);
        String port = Optional.ofNullable(System.getenv("DB_PORT")).filter(p -> !p.isEmpty()).orElse(DEFAULT_DATABASE_PORT);
        String name = Optional.ofNullable(System.getenv("DB_NAME")).filter(n -> !n.isEmpty()).orElse(DEFAULT_DATABASE_NAME);
        String user = Optional.ofNullable(System.getenv("DB_USER")).filter(u -> !u.isEmpty()).orElse(DEFAULT_DATABASE_USER);
        String pass = Optional.ofNullable(System.getenv("DB_PASS")).filter(p -> !p.isEmpty()).orElse(DEFAULT_DATABASE_PASS);
        SQLDriver driver = new PostgreSQLDriver();
        SQLSettings settings = driver.getDefaultSettings();
        settings.host(host);
        settings.port(port);
        settings.database(name);
        settings.username(user);
        settings.password(pass);
        this.client = new JavaSQLClient(settings, driver);
    }

    public static SQLClient getSQLClient() {
        if (instance == null) {
            instance = new DatabaseContext();
        }
        return instance.client;
    }
}
