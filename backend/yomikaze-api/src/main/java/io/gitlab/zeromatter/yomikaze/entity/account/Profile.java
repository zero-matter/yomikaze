package io.gitlab.zeromatter.yomikaze.entity.account;

import io.gitlab.k4zoku.core.database.sql.annotation.Column;
import io.gitlab.k4zoku.core.database.sql.annotation.PrimaryKey;
import io.gitlab.k4zoku.core.database.sql.annotation.Table;
import lombok.Data;

import java.sql.Blob;

@Data
@Table
public class Profile {
    private @Column
    @PrimaryKey int accountId;
    private @Column String displayName;
    private @Column Blob avatar;
    private @Column String bio;
}
