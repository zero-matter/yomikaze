package io.gitlab.zeromatter.yomikaze.api;

import io.gitlab.k4zoku.core.http.SafeHttpServlet;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class RestfulServlet extends SafeHttpServlet {

    protected void initResponse(HttpServletResponse response) {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
    }

    protected void send(HttpServletResponse response, String status, String message, JsonObject... data) throws IOException {
        initResponse(response);
        JsonObjectBuilder builder = Json.createObjectBuilder()
                .add("status", status)
                .add("message", message);
        if (data.length > 0) {
            if (data.length == 1) {
                builder.add("data", data[0]);
            } else {
                builder.add("data", Json.createArrayBuilder(Arrays.asList(data)).build());
            }
        }
        response.getWriter().write(builder.build().toString());
    }

    protected void sendSuccess(HttpServletResponse response, String message, JsonObject... data) throws IOException {
        send(response, "success", message, data);
    }

    protected void sendError(HttpServletResponse response, String message, JsonObject... data) throws IOException {
        send(response, "error", message, data);
    }
}
