package io.gitlab.zeromatter.yomikaze.api.authentication;

import io.gitlab.k4zoku.core.util.Validate;
import io.gitlab.zeromatter.yomikaze.api.RestfulServlet;
import io.gitlab.zeromatter.yomikaze.entity.account.Account;
import io.gitlab.zeromatter.yomikaze.entity.account.AccountDataAccess;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/sign-up")
public class SignUp extends RestfulServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String username = req.getParameter("username");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String passwordConfirmation = req.getParameter("password-confirmation");

        try {
            Validate.validateNotEmpty(username, "Username cannot be empty");
            Validate.validateNotEmpty(email, "Email cannot be empty");
            Validate.validateNotEmpty(password, "Password cannot be empty");
            Validate.validateNotEmpty(passwordConfirmation, "Password confirmation cannot be empty");
            Validate.validateEquals(password, passwordConfirmation, "Password and password confirmation must match");
        } catch (IllegalArgumentException e) {
            sendError(resp, e.getMessage());
            return;
        }

        if (password.equals(passwordConfirmation)) {
            Account account = new Account();
            account.setUsername(username);
            account.setEmail(email);
            account.setRawPassword(password);
            account.setTwoFactorEnabled(false);
            AccountDataAccess dataAccess = new AccountDataAccess();
            dataAccess.save(account);
            new SignIn().doPost(req, resp);
        } else {
            sendError(resp, "Password and password confirmation must match");
        }
    }
}
