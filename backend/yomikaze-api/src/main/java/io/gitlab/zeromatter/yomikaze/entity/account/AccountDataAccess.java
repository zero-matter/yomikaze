package io.gitlab.zeromatter.yomikaze.entity.account;

import io.gitlab.k4zoku.core.util.Validate;
import io.gitlab.zeromatter.yomikaze.entity.YomikazeAccessObject;

public class AccountDataAccess extends YomikazeAccessObject<Account> {
    public AccountDataAccess() {
        super();
    }

    public AccountDataAccess(String table) {
        super(table);
    }

    public Account signUp(String username, String email, String password) {
        Account account = new Account();
        account.setUsername(username);
        account.setEmail(email);
        account.setRawPassword(password);
        account.setTwoFactorEnabled(false);
        save(account);
        return account;
    }

    public Account signIn(String usernameOrEmail, String password) {
        Account account = find(usernameOrEmail);
        Validate.validateNotNull(account, "Account not found");
        Validate.validateTrue(account.verifyPassword(password), "Password incorrect");
        return account;
    }

    public void signOut(Account account) {
        account.invalidate();
        update(account);
    }
}
