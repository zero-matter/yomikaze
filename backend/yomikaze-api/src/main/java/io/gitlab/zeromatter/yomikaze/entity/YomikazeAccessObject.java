package io.gitlab.zeromatter.yomikaze.entity;

import io.gitlab.k4zoku.core.database.sql.data.SQLAccessObject;
import io.gitlab.k4zoku.core.database.sql.data.SQLTransferObject;
import io.gitlab.zeromatter.yomikaze.database.DatabaseContext;

public abstract class YomikazeAccessObject<T extends SQLTransferObject> extends SQLAccessObject<T> {
    protected YomikazeAccessObject() {
        super(DatabaseContext.getSQLClient());
    }

    protected YomikazeAccessObject(String table) {
        super(DatabaseContext.getSQLClient(), table);
    }
}
