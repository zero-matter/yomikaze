package io.gitlab.zeromatter.yomikaze.api.authentication;

import io.gitlab.k4zoku.core.util.Validate;
import io.gitlab.zeromatter.yomikaze.api.RestfulServlet;
import io.gitlab.zeromatter.yomikaze.entity.account.Account;
import io.gitlab.zeromatter.yomikaze.entity.account.AccountDataAccess;

import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/sign-in")
public class SignIn extends RestfulServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        try {
            Validate.validateNotEmpty(username, "Username cannot be empty");
            Validate.validateNotEmpty(password, "Password cannot be empty");
        } catch (IllegalArgumentException e) {
            sendError(resp, e.getMessage());
            return;
        }
        try {
            Account account = new AccountDataAccess().signIn(username, password);
            String token = account.generateToken();
            resp.setHeader("Authorization", "Bearer " + token);
            if (req.getParameter("remember") != null) {
                resp.addCookie(new Cookie("token", token));
            }
            req.getSession().setAttribute("token", token);
            req.getSession().setAttribute("account", account);
            if (req.getParameter("redirect") != null) {
                resp.sendRedirect(req.getParameter("redirect"));
            } else {
                JsonObject json = Json.createObjectBuilder()
                        .add("token", token)
                        .add("username", account.getUsername())
                        .add("email", account.getEmail())
                        .build();
                sendSuccess(resp, "Signed in successfully", json);
            }
        } catch (IllegalArgumentException e) {
            sendError(resp, e.getMessage());
            return;
        }
        sendError(resp, "Invalid username or password");
    }
}
