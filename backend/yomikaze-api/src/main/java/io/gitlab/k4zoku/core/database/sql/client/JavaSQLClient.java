package io.gitlab.k4zoku.core.database.sql.client;

import io.gitlab.k4zoku.core.database.sql.base.SQLClient;
import io.gitlab.k4zoku.core.database.sql.base.SQLDriver;
import io.gitlab.k4zoku.core.database.sql.base.SQLSettings;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaSQLClient extends SQLClient {

    private static final Logger LOGGER = Logger.getLogger(JavaSQLClient.class.getName());
    private final Properties properties;
    private final Queue<Connection> connections;

    public JavaSQLClient(SQLSettings settings, SQLDriver driver) {
        super(settings, driver);
        this.properties = new Properties(settings.getClientProperties());
        this.properties.setProperty("user", settings.getUsername());
        this.properties.setProperty("password", settings.getPassword());
        this.connections = new LinkedList<>();
    }

    @SuppressWarnings("unused")
    @Override
    public Connection getConnection() throws SQLException {
        Connection connection;
        try {
            Driver driver = this.driver.getDriverClass().getConstructor().newInstance();
            connection = driver.connect(this.driver.createUrl(settings), properties);
        } catch (NoSuchMethodException | SecurityException | InstantiationException |
                 IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, "Cannot get connection from driver class", ex);
            connection = DriverManager.getConnection(this.driver.createUrl(settings), properties);
        }
        connections.add(connection);
        return connection;
    }

    @Override
    public void close() {
        while (!connections.isEmpty()) {
            Connection connection = connections.poll();
            try {
                connection.close();
            } catch (SQLException ex) {
                LOGGER.log(Level.WARNING, ex, () -> "Cannot close connection " + connection);
            }
        }
    }
}
