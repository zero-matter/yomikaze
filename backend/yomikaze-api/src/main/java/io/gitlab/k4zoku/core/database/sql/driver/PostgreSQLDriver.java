package io.gitlab.k4zoku.core.database.sql.driver;

import io.gitlab.k4zoku.core.database.sql.base.SQLDriver;
import io.gitlab.k4zoku.core.database.sql.base.SQLSettings;

import java.sql.Driver;

public class PostgreSQLDriver implements SQLDriver {

    @SuppressWarnings("unchecked")
    @Override
    public Class<? extends Driver> getDriverClass() {
        try {
            return org.postgresql.Driver.class;
        } catch (NoClassDefFoundError | ExceptionInInitializerError e) {
            try {
                return (Class<? extends Driver>) Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                throw new IllegalStateException("Could not find the driver class.", ex);
            }
        }
    }

    @Override
    public SQLSettings getDefaultSettings() {
        SQLSettings settings = new SQLSettings();
        settings.host("localhost");
        settings.port("5432");
        settings.database("postgres");
        settings.username("postgres");
        settings.password("");
        return settings;
    }

    @Override
    public String getJDBCDriverName() {
        return "postgresql";
    }

    @Override
    public String createUrl(SQLSettings settings) {
        StringBuilder builder = new StringBuilder();
        builder.append("jdbc:").append(getJDBCDriverName());
        builder.append("://").append(settings.getHost());
        builder.append(':').append(settings.getPort());
        builder.append("/");
        settings.getDatabase().ifPresent(builder::append);
        builder.append(settings.getDriverPropertiesString("?", "&"));
        return builder.toString();
    }
}