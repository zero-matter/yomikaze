package io.gitlab.k4zoku.core.database.sql.functions;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetConsumer {
    void accept(ResultSet resultSet) throws SQLException;
}
