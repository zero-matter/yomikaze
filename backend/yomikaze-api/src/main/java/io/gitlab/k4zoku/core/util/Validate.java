package io.gitlab.k4zoku.core.util;

import java.util.List;
import java.util.Objects;

@SuppressWarnings("unused")
public class Validate {
    private Validate() {
        // Utility class
    }

    public static void validateNotNull(Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotNull(Object object) {
        validateNotNull(object, "Object cannot be null");
    }

    public static void validateNotEmpty(String string, String message) {
        if (string == null || string.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotEmpty(String string) {
        validateNotEmpty(string, "String cannot be empty");
    }

    public static <T> void validateNotEmpty(T[] array, String message) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(message);
        }
    }

    public static <T> void validateNotEmpty(T[] array) {
        validateNotEmpty(array, "Array cannot be empty");
    }

    public static void validateNotEmpty(List<?> list, String message) {
        if (list == null || list.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotEmpty(List<?> list) {
        validateNotEmpty(list, "List cannot be empty");
    }

    public static void validateTrue(boolean condition, String message) {
        if (!condition) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateTrue(boolean condition) {
        validateTrue(condition, "Condition must be true");
    }

    public static void validateFalse(boolean condition, String message) {
        if (condition) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateFalse(boolean condition) {
        validateFalse(condition, "Condition must be false");
    }

    public static void validateNull(Object object, String message) {
        if (object != null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNull(Object object) {
        validateNull(object, "Object must be null");
    }

    public static void validateEquals(int a, int b, String message) {
        if (a != b) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateEquals(int a, int b) {
        validateEquals(a, b, "Values must be equal");
    }

    public static void validateEquals(long a, long b, String message) {
        if (a != b) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateEquals(long a, long b) {
        validateEquals(a, b, "Values must be equal");
    }

    public static void validateEquals(float a, float b, String message) {
        if (a != b) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateEquals(float a, float b) {
        validateEquals(a, b, "Values must be equal");
    }

    public static void validateEquals(double a, double b, String message) {
        if (a != b) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateEquals(double a, double b) {
        validateEquals(a, b, "Values must be equal");
    }

    public static void validateEquals(Object a, Object b, String message) {
        if (Objects.equals(a, b)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateEquals(Object a, Object b) {
        validateEquals(a, b, "Values must be equal");
    }

    public static void validateNotEquals(int a, int b, String message) {
        if (a == b) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotEquals(int a, int b) {
        validateNotEquals(a, b, "Values must not be equal");
    }

    public static void validateNotEquals(long a, long b, String message) {
        if (a == b) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotEquals(long a, long b) {
        validateNotEquals(a, b, "Values must not be equal");
    }

    public static void validateNotEquals(float a, float b, String message) {
        if (a == b) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotEquals(float a, float b) {
        validateNotEquals(a, b, "Values must not be equal");
    }

    public static void validateNotEquals(double a, double b, String message) {
        if (a == b) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotEquals(double a, double b) {
        validateNotEquals(a, b, "Values must not be equal");
    }

    public static void validateNotEquals(Object a, Object b, String message) {
        if (!Objects.equals(a, b)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateNotEquals(Object a, Object b) {
        validateNotEquals(a, b, "Values must not be equal");
    }
}
