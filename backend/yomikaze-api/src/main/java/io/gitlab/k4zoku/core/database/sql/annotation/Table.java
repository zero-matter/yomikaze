package io.gitlab.k4zoku.core.database.sql.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Table {
    /**
     * The name of the table. If not specified, the name of the class will be used.
     *
     * @return The name of the table.
     */
    String value() default "";
}
