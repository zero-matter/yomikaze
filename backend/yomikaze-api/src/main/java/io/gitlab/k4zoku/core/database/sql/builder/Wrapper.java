package io.gitlab.k4zoku.core.database.sql.builder;

@SuppressWarnings("unused")
public interface Wrapper {
    default String wrapField(String field) {
        return field;
    }

    default String wrapTable(String table) {
        return table;
    }
}
