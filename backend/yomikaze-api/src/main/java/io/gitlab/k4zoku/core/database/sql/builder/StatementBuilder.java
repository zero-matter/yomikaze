package io.gitlab.k4zoku.core.database.sql.builder;

@SuppressWarnings("unused")
public abstract class StatementBuilder {

    private static Wrapper globalWrapper = new Wrapper() {
    };
    protected Wrapper wrapper;

    protected StatementBuilder(Wrapper wrapper) {
        this.wrapper = wrapper;
    }

    protected StatementBuilder() {
        this.wrapper = getGlobalWrapper();
    }

    public static Wrapper getGlobalWrapper() {
        return globalWrapper;
    }

    public static void setGlobalWrapper(Wrapper wrapper) {
        if (wrapper == null) throw new IllegalArgumentException("Wrapper cannot be null");
        globalWrapper = wrapper;
    }

    public Wrapper getWrapper() {
        return wrapper;
    }

    public StatementBuilder setWrapper(Wrapper wrapper) {
        if (wrapper == null) throw new IllegalArgumentException("Wrapper cannot be null");
        this.wrapper = wrapper;
        return this;
    }

    protected abstract SQLStatement build();

}
