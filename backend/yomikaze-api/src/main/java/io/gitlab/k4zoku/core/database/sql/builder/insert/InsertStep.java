package io.gitlab.k4zoku.core.database.sql.builder.insert;

import io.gitlab.k4zoku.core.util.Validate;

import java.util.List;

public class InsertStep implements Insert {

    private final StringBuilder builder;

    InsertStep() {
        this.builder = new StringBuilder();
        builder.append("INSERT");
    }

    @Override
    public InsertIntoStep into(String table) {
        Validate.validateNotEmpty(table, "Table name cannot be empty");
        return new InsertIntoStep(builder, table);
    }

    @Override
    public InsertColumnsStep into(String table, String... columns) {
        Validate.validateNotEmpty(table, "Table name cannot be empty");
        Validate.validateNotEmpty(columns, "Column names cannot be empty");
        return new InsertIntoStep(builder, table).columns(columns);
    }

    @Override
    public InsertColumnsStep into(String table, List<String> columns) {
        Validate.validateNotEmpty(table, "Table name cannot be empty");
        Validate.validateNotEmpty(columns, "Column names cannot be empty");
        return new InsertIntoStep(builder, table).columns(columns);
    }
}
