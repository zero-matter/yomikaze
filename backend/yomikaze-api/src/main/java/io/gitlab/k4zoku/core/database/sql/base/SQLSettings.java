package io.gitlab.k4zoku.core.database.sql.base;

import lombok.Getter;

import java.util.Optional;
import java.util.Properties;

/**
 * @author Kazoku
 * @version 1.0
 * <p>
 * This class is used to define the settings that will
 * be used to connect to the database.
 * @since 2022-07-01
 */
@Getter
@SuppressWarnings({"unused", "UnusedReturnValue"})
public class SQLSettings {
    private final Properties clientProperties;
    private final Properties driverProperties;
    private String host;
    private String port;
    private String username;
    private String password;
    private String database;

    public SQLSettings(SQLSettings settings) {
        this.clientProperties = new Properties(settings.getClientProperties());
        this.driverProperties = new Properties(settings.getDriverProperties());
        this.host = settings.getHost();
        this.port = settings.getPort();
        this.username = settings.getUsername();
        this.password = settings.getPassword();
        this.database = settings.getDatabase().orElse(null);
    }

    public SQLSettings() {
        this.clientProperties = new Properties();
        this.driverProperties = new Properties();
        this.host = "127.0.0.1";
        this.port = "3306";
        this.username = "root";
        this.password = "";
        this.database = null;
    }

    public Properties getClientProperties() {
        return new Properties(this.clientProperties);
    }

    public Properties getDriverProperties() {
        return new Properties(this.driverProperties);
    }
    public SQLSettings setClientProperty(String key, String value) {
        this.clientProperties.setProperty(key, value);
        return this;
    }
    public SQLSettings setDriverProperty(String key, String value) {
        this.driverProperties.setProperty(key, value);
        return this;
    }

    public SQLSettings host(String host) {
        this.host = host;
        return this;
    }

    public SQLSettings port(String port) {
        this.port = port;
        return this;
    }

    public SQLSettings username(String username) {
        this.username = username;
        return this;
    }

    public SQLSettings password(String password) {
        this.password = password;
        return this;
    }

    public SQLSettings database(String database) {
        this.database = database;
        return this;
    }

    public Optional<String> getDatabase() {
        return Optional.ofNullable(this.database);
    }

    public String getDriverPropertiesString(String prefix, String delimiter) {
        StringBuilder sb = new StringBuilder();
        sb.append(prefix);
        for (String key : this.driverProperties.stringPropertyNames()) {
            sb.append(key);
            sb.append('=');
            sb.append(this.driverProperties.getProperty(key));
            sb.append(delimiter);
        }
        sb.delete(sb.length() - delimiter.length(), sb.length());
        return sb.toString();
    }
}
