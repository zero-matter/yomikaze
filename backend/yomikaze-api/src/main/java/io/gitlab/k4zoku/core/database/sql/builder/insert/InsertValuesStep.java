package io.gitlab.k4zoku.core.database.sql.builder.insert;

import io.gitlab.k4zoku.core.database.sql.builder.SQLStatement;

import java.util.List;

public class InsertValuesStep implements Insert {
    private final StringBuilder builder;
    private Object[] values;

    InsertValuesStep(StringBuilder builder) {
        this.builder = builder;
    }

    @Override
    public SQLStatement values(Object... values) {
        this.values = values;
        return build();
    }

    @Override
    public SQLStatement values(List<Object> values) {
        this.values = values.toArray(new Object[0]);
        return build();
    }

    private SQLStatement build() {
        return new SQLStatement(builder.toString(), values);
    }


}
