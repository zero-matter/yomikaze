package io.gitlab.k4zoku.core.database.sql.builder.insert;

import io.gitlab.k4zoku.core.util.Validate;

import java.util.List;

public class InsertIntoStep implements Insert {
    private final StringBuilder builder;

    InsertIntoStep(StringBuilder builder, String table) {
        Validate.validateNotNull(builder, "Builder cannot be null");
        Validate.validateNotEmpty(table, "Table name cannot be empty");
        this.builder = builder;
        builder.append(" INTO ");
        builder.append(table);
    }

    @Override
    public InsertColumnsStep columns(String... columns) {
        Validate.validateNotEmpty(columns, "Column names cannot be empty");
        return new InsertColumnsStep(builder).columns(columns);
    }

    @Override
    public InsertColumnsStep columns(List<String> columns) {
        Validate.validateNotEmpty(columns, "Column names cannot be empty");
        return new InsertColumnsStep(builder).columns(columns);
    }
}
