package io.gitlab.k4zoku.core.database.sql.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    /**
     * The name of the column. If not specified, the name of the field will be used.
     *
     * @return The name of the column.
     */
    String value() default "";

    /**
     * Index of the column. If negative, the column will index by field order.
     *
     * @return Index of the column.
     */
    int index() default -1;
}
