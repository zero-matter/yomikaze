package io.gitlab.k4zoku.core.database.sql.builder;

import io.gitlab.k4zoku.core.database.sql.functions.ResultSetConsumer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.List;
import java.util.function.IntConsumer;

@SuppressWarnings("unused")
public class PrepareState {

    private final PreparedStatement statement;
    private ResultSetConsumer onQuery = unused -> {
    };
    private ResultSetConsumer onAutoGeneratedKeys = unused -> {
    };
    private IntConsumer onUpdate = unused -> {
    };

    public PrepareState(PreparedStatement statement) {
        this.statement = statement;
    }

    public PreparedStatement getStatement() {
        return statement;
    }

    public PrepareState queried(ResultSetConsumer onQuery) {
        this.onQuery = onQuery;
        return this;
    }

    public PrepareState bind(Object... parameters) throws SQLException {
        for (int i = 0; i < parameters.length; i++) {
            statement.setObject(i + 1, parameters[i]);
        }
        return this;
    }

    public PrepareState bind(List<Object> parameters) throws SQLException {
        for (int i = 0; i < parameters.size(); i++) {
            statement.setObject(i + 1, parameters.get(i));
        }
        return this;
    }

    public PrepareState keysGenerated(ResultSetConsumer onAutoGeneratedKeys) {
        this.onAutoGeneratedKeys = onAutoGeneratedKeys;
        return this;
    }

    public PrepareState updated(IntConsumer onUpdate) {
        this.onUpdate = onUpdate;
        return this;
    }

    public void execute() throws SQLException {
        if (statement.execute()) {
            ResultSet result = statement.getResultSet();
            if (onQuery != null && result != null) {
                onQuery.accept(result);
                result.close();
            }
        } else {
            int updateCount = statement.getUpdateCount();
            if (onUpdate != null && updateCount != -1) onUpdate.accept(updateCount);
        }

        ResultSet generatedKeys;
        try {
            generatedKeys = statement.getGeneratedKeys();
        } catch (SQLFeatureNotSupportedException e) {
            generatedKeys = null;
        }
        if (onAutoGeneratedKeys != null && generatedKeys != null) {
            onAutoGeneratedKeys.accept(generatedKeys);
            generatedKeys.close();
        }
        statement.close();
    }
}
