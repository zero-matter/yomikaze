package io.gitlab.k4zoku.core.database.sql.base;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Kazoku
 * @version 1.0
 * <p>
 * A SQL client is used to connect to the database and execute SQL queries.
 * It is also used to get the original object that was used to create the client.
 * @since 2022-07-01
 */
@SuppressWarnings("ALL")
public abstract class SQLClient implements AutoCloseable {

    private static SQLClient instance;
    protected final SQLSettings settings;
    protected final SQLDriver driver;

    protected SQLClient(SQLSettings settings, SQLDriver driver) {
        this.settings = settings;
        this.driver = driver;
    }

    public static SQLClient getInstance() {
        return instance;
    }

    public static void setInstance(SQLClient instance) {
        SQLClient.instance = instance;
    }

    public SQLDriver getDriver() {
        return this.driver;
    }

    public SQLSettings getSettings() {
        return this.settings;
    }

    public abstract Connection getConnection() throws SQLException;

    @Override
    public abstract void close();
}
