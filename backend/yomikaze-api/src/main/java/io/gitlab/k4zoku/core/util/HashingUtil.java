package io.gitlab.k4zoku.core.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashingUtil {

    private HashingUtil() {
        // Utility class
    }

    public static String hex(byte[] array) {
        StringBuilder sb = new StringBuilder();
        for (byte b : array) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100));
            sb.deleteCharAt(sb.length() - 3);
        }
        return sb.toString();
    }

    @SuppressWarnings("unused")
    public static String md5(String message) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            return hex(md.digest(message.getBytes("CP1252")));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | NullPointerException ignored) {
            return "";
        }
    }
}
