package io.gitlab.k4zoku.core.database.sql.builder.insert;

import io.gitlab.k4zoku.core.database.sql.builder.SQLStatement;
import io.gitlab.k4zoku.core.util.Validate;

import java.util.List;

@SuppressWarnings("unused")
public class InsertColumnsStep implements Insert {
    private final StringBuilder builder;
    private String[] columns;

    InsertColumnsStep(StringBuilder builder) {
        this.builder = builder;
    }

    @Override
    public InsertColumnsStep columns(String... columns) {
        this.columns = columns;
        buildColumns();
        return this;
    }

    @Override
    public InsertColumnsStep columns(List<String> columns) {
        this.columns = columns.toArray(new String[0]);
        buildColumns();
        return this;
    }

    private void buildColumns() {
        Validate.validateNotEmpty(columns, "Column names cannot be empty");
        builder.append(" (");
        for (int i = 0; i < columns.length; i++) {
            if (i > 0) builder.append(", ");
            Validate.validateNotEmpty(columns[i], "Column name cannot be empty");
            builder.append(columns[i]);
        }
        builder.append(") VALUES (");
        for (int i = 0; i < columns.length; i++) {
            if (i > 0) builder.append(", ");
            builder.append("?");
        }
        builder.append(")");
    }

    @Override
    public SQLStatement values(Object... values) {
        return new InsertValuesStep(builder).values(values);
    }

    @Override
    public SQLStatement values(List<Object> values) {
        return new InsertValuesStep(builder).values(values);
    }
}
