package io.gitlab.k4zoku.core.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionUtil {

    private ReflectionUtil() {
        // Utility class
    }

    public static Object getFieldValue(Object object, Field field) {
        Validate.validateNotNull(object, "Object cannot be null");
        Validate.validateNotNull(field, "Field cannot be null");
        Validate.validateTrue(field.getDeclaringClass().isAssignableFrom(object.getClass()), "Field must be declared in the same class or superclass");
        String getterName = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
        try {
            Method getter = object.getClass().getMethod(getterName);
            return getter.invoke(object);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            try {
                field.setAccessible(true);
                return field.get(object);
            } catch (IllegalAccessException ex) {
                throw new IllegalStateException("Could not get field value", ex);
            }
        }
    }

    public static void setFieldValue(Object object, Field field, Object value) {
        Validate.validateNotNull(object, "Object cannot be null");
        Validate.validateNotNull(field, "Field cannot be null");
        Validate.validateTrue(field.getDeclaringClass().isAssignableFrom(object.getClass()), "Field must be declared in the same class or superclass");
        String setterName = "set" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
        try {
            Method setter = object.getClass().getMethod(setterName, field.getType());
            setter.setAccessible(true);
            setter.invoke(object, value);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            field.setAccessible(true);
            try {
                field.set(object, value);
            } catch (IllegalAccessException ex) {
                throw new IllegalStateException("Cannot set field value", ex);
            }
        }
    }

    public static void setFieldsValues(Object object, Field[] fields, Object[] values) {
        Validate.validateNotEmpty(fields, "Fields cannot be empty");
        Validate.validateNotEmpty(values, "Values cannot be empty");
        Validate.validateEquals(fields.length, values.length, "Fields and values must be of the same length");
        for (int i = 0; i < fields.length; i++) {
            Validate.validateTrue(fields[i].getDeclaringClass().isAssignableFrom(object.getClass()), "Field must be declared in the same class or superclass");
            Validate.validateTrue(autoBoxing(fields[i].getType()).isAssignableFrom(autoBoxing(values[i].getClass())), "Value type must be assignable to field type");
            setFieldValue(object, fields[i], values[i]);
        }
    }

    public static Class<?> autoBoxing(Class<?> clazz) {
        Validate.validateNotNull(clazz, "Class cannot be null");
        if (clazz.isPrimitive()) {
            if (clazz.equals(boolean.class)) {
                return Boolean.class;
            } else if (clazz.equals(byte.class)) {
                return Byte.class;
            } else if (clazz.equals(char.class)) {
                return Character.class;
            } else if (clazz.equals(short.class)) {
                return Short.class;
            } else if (clazz.equals(int.class)) {
                return Integer.class;
            } else if (clazz.equals(long.class)) {
                return Long.class;
            } else if (clazz.equals(float.class)) {
                return Float.class;
            } else if (clazz.equals(double.class)) {
                return Double.class;
            }
        }
        return clazz;
    }
}
