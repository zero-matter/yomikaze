package io.gitlab.k4zoku.core.database;

import java.util.List;

@SuppressWarnings("unused")
public interface DataAccessObject<E extends DataTransferObject> {

    /**
     * Synchronizes the given entity with the database. (Database -> Object)
     *
     * @param entity The entity to synchronize.
     */
    void sync(E entity);

    /**
     * Saves (insert) the given entity to the database. (Object -> Database)
     * on update, use {@link #update(DataTransferObject)} instead.
     *
     * @param entity The entity to save.
     */
    void save(E entity);

    /**
     * Removes the given entity from the database.
     *
     * @param entity The entity to remove.
     */
    void remove(E entity);

    /**
     * Removes the given entity from the database.
     *
     * @param <K>        The type of the identifier.
     * @param identifier The identifier of the entity to remove.
     */
    <K> void remove(K identifier);

    /**
     * Updates the given entity in the database. (Object -> Database)
     *
     * @param entity The entity to update.
     */
    void update(E entity);

    /**
     * Finds an entity by its identifier. (Database -> Object)
     *
     * @param <K>        The type of the identifier.
     * @param identifier The identifier of the entity to find.
     * @return The entity with the given identifier.
     */
    <K> E find(K identifier);

    /**
     * Finds all entities. (Database -> Object)
     *
     * @param limit  The maximum number of entities to find.
     * @param offset The number of entities to skip.
     * @return A list of all entities.
     */
    List<E> list(int limit, int offset);

}
