package io.gitlab.k4zoku.core.database.sql.builder.insert;

import io.gitlab.k4zoku.core.database.sql.builder.SQLStatement;

import java.util.List;

@SuppressWarnings("ALL")
public interface Insert {

    static InsertStep insert() {
        return new InsertStep();
    }

    static InsertIntoStep insertInto(String table) {
        return new InsertStep().into(table);
    }

    static InsertColumnsStep insertInto(String table, String... columns) {
        return new InsertStep().into(table, columns);
    }

    static InsertColumnsStep insertInto(String table, List<String> columns) {
        return new InsertStep().into(table, columns);
    }

    default Insert into(String table) {
        throw new UnsupportedOperationException("Not this step");
    }

    default Insert into(String table, String... columns) {
        throw new UnsupportedOperationException("Not this step");
    }

    default Insert into(String table, List<String> columns) {
        throw new UnsupportedOperationException("Not this step");
    }

    default Insert columns(String... columns) {
        throw new UnsupportedOperationException("Not this step");
    }

    default Insert columns(List<String> columns) {
        throw new UnsupportedOperationException("Not this step");
    }

    default SQLStatement values(Object... values) {
        throw new UnsupportedOperationException("Not this step");
    }

    default SQLStatement values(List<Object> values) {
        throw new UnsupportedOperationException("Not this step");
    }
}
